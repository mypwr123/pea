﻿#include <iostream>
#include <fstream>
#include <chrono>												//czas
#include <random>												//random

using namespace std;
using namespace chrono;

int** macierz;													// macierz
int n;															// wierzcholki
																// graf 
class graf {
public:
	graf(int wierzcholki);
	void dodaj(int v1, int v2);
	void pokagraf();
	void BFS(int poczatkowa);
	void DFS(int poczatkowa);
	void usungraf(); 
};

graf::graf(int wierzcholki) {
	n = wierzcholki;
																// rezerwuje pamięć na macierz 
	macierz = new int* [n];
	for (int i = 0; i < n; ++i)
		macierz[i] = new int[n];
																// wypełnianie macierzy zerami
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			macierz[i][j] = 0;
}

void graf::dodaj(int v1, int v2) {
																// dodaje 1 w danym miejscu w macierzy 
	macierz[v1][v2] = 1;
	macierz[v2][v1] = 1;
}

void graf::pokagraf() {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j)
			cout << macierz[i][j] << " ";
		cout << endl;
	}
}

void graf::BFS(int poczatkowa) {
	bool* odwiedzone = new bool[n];
	for (int i = 0; i < n; ++i)
		odwiedzone[i] = false;

																// kolejka
	int* kolejka = new int[n];
	int front = -1, rear = -1;

	odwiedzone[poczatkowa] = true;
	kolejka[++rear] = poczatkowa;

	while (front != rear) {
		poczatkowa = kolejka[++front];

		for (int i = 0; i < n; ++i) {
			if (macierz[poczatkowa][i] == 1 && !odwiedzone[i]) {
				odwiedzone[i] = true;
				kolejka[++rear] = i;
			}
		}
	}

	delete[] odwiedzone;
	delete[] kolejka;
}

void graf::DFS(int poczatkowa) {
	bool* odwiedzone = new bool[n];
	for (int i = 0; i < n; ++i)
		odwiedzone[i] = false;
																 // stos
	int* stos = new int[n];
	int top = -1;

	odwiedzone[poczatkowa] = true;
	stos[++top] = poczatkowa;

	while (top != -1) {
		poczatkowa = stos[top--];

		for (int i = 0; i < n; ++i) {
			if (macierz[poczatkowa][i] == 1 && !odwiedzone[i]) {
				odwiedzone[i] = true;
				stos[++top] = i;
			}
		}
	}

	delete[] odwiedzone;
	delete[] stos;
}

void graf::usungraf() {
	for (int i = 0; i < n; ++i)
	delete[] macierz[i];
	delete[] macierz;
}

int main() {
																			// parametry grafu
	int wielkosci[] = { 10,20 };
	double gestosci[] = { 0.25, 0.5 };
																			// plik z pomiarami .csv
	ofstream pomiary("pomiary.csv");
	pomiary << "graf wielkosc,graf gestosc,BFS Time (seconds),DFS Time (seconds)\n";

	for (int i = 0; i < sizeof(wielkosci) / sizeof(wielkosci[0]); ++i) {
		n = wielkosci[i];
		for (int j = 0; j < sizeof(gestosci) / sizeof(gestosci[0]); ++j) {
			double gestosc = gestosci[j];

			for (int k = 0; k < 1; ++k) {									//pętla do pomiarów 

				graf graf(n);
																			// liczenie krawędzi
				int maxkrawedzi = (n * (n - 1)) / 2;
				int krawedzie = gestosc * maxkrawedzi;

				random_device rd;											//lizcby losowe itp
				mt19937 gen(rd());
				uniform_int_distribution<int> distrib(0, n - 1);
																			// generowanie losowych macierzy 
				for (int l = 0; l < krawedzie; ++l) {
					int v1 = distrib(gen);
					int v2 = distrib(gen);
					if (v1 == v2 || macierz[v1][v2] == 1) {
						--l;
					}
					else {
						graf.dodaj(v1, v2);
					}
				}

				 graf.pokagraf();

				cout << endl << " wielkosc grafu: " << n << " gestosc grafu: " << gestosc << endl << endl;

																								//  BFS
				auto t1BFS = high_resolution_clock::now();
				graf.BFS(0);
				auto t2BFS = high_resolution_clock::now();
				double t3BFS = duration_cast<duration<double>>(t2BFS - t1BFS).count();

																								// DFS
				auto t1DFS = high_resolution_clock::now();
				graf.DFS(0);
				auto t2DFS = high_resolution_clock::now();
				double t3DFS = duration_cast<duration<double>>(t2DFS - t1DFS).count();

																								// zapisywanie do pomiarów 
				pomiary << n << "," << gestosc << ","
					<< t3BFS << "," << t3DFS << endl;

				graf.usungraf();
			}
		}
	}

	pomiary.close();
	return 0;
}
