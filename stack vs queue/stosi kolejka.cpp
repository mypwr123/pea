﻿#include <iostream>
#include <Windows.h>
#include <fstream>
#include <iomanip>			//czas
#include <string>	

using namespace std;

fstream myfile;					//plik z elementami grafu
ofstream stosfile;				//plik z zapisanymi pomiarami czasu dla algorytmu Dijkstry dla listy
ofstream kolfile;				//plik z zapisanymi pomiarami czasu dla algorytmu Dijkstru dla macierzy

double elapsed4, elapsed5, elapsed6, elapsed, elapsed2, elapsed3, start;	//zmienne pomiaru czasu
long long int  freq = 1000;

int U;								//sztuwna ilosc elementów
long long int read_QPC()
{
	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return((long long int)count.QuadPart);
}

struct estos						//element stosu
{
	int wartosc = 0;
	estos* next = NULL;				//wskaznik na popszedni element stosu
};

estos* mystos;						//glowny stos i pomocniczy
estos* tempstos;


void Wpisz()						//wpisywanie wartosci do pliku
{
	int N = U;						//elemnty 
	int M = 10000;					//maksymalna warotsc elementow
	int a;

	srand(time(NULL));							//inicjowanie pomiaru czasu

	myfile.open("DaneLosowe.txt", fstream::out); //czyszczenie pliku z danymi
	myfile.close();
	myfile.open("DaneLosowe.txt");

	//cout << "Ile ma być elementów: ";


	//cout << "maksymalna wartosc przechowywyana w stosie: ";


	myfile << M << ' ' << N << endl;			//W pierwszym wierszu pliku zapisana jest ilosc elementow poczatkowych stosu

	int max = M;
	for (int i = 1; i <= N; i++)
	{
		a = rand() % max;				//wartosc elementow
		myfile << a << endl;			//wpisywanie elementu
	}

	myfile.close();
}


void usungornystos(estos*& top)
{
	start = read_QPC();								//poczatek timera
	estos* temp = top;
	top = (top)->next;
	delete temp;
	elapsed2 = (read_QPC() - start) / (float)(freq);  //koniec timera
}


void pokastos(estos* top) {

	while (top != NULL) {							// wypisuje do poki jest cos na stosie 
		cout << top->wartosc << endl;
		top = top->next;
	}
	cout << "NULL" << endl << endl;
}


void szukaniestos(estos*& top) {


}

void usunostatnistos(estos*& top) {
	start = read_QPC();								//poczatek timera
	do {											//zdejmuje az do ostatniego elementu stosu 1
		estos* newElement = new estos;				//tworzy newelement 
		newElement->wartosc = top->wartosc;			//przepisuje wartosc ze stosu 1 do newelement
		newElement->next = tempstos;				//łaczy z 2gim stosem
		tempstos = newElement;						//dodaje do 2sotsu

		estos* temp = top;							//usuwa pierwszy element stosu 1
		top = top->next;
		delete temp;

	} while (top->next != NULL);


	estos* temp = top;								//usuwa ostatni element który został
	top = top->next;								//i z przedostatniego jest robiony ostatyni
	delete temp;
	top = NULL;

	while (tempstos != NULL) {						//stos jest zapełniany wartościami ze stodu 2
		estos* newElement = new estos;
		newElement->wartosc = tempstos->wartosc;
		newElement->next = top;
		top = newElement;

		estos* temp = tempstos;
		tempstos = tempstos->next;
		delete temp;
	}
	elapsed = (read_QPC() - start) / (float)(freq);  //koniec timera
}


void czyszczeniestos(estos*& top) {
	if (top != NULL) {
		estos* temp = top;
		top = (top)->next;
		delete temp;
	}
}

void stos(estos*& top)
{
	myfile.open("DaneLosowe.txt");

	if (myfile)
	{
		int N;					//elemnty 
		int M;					//maksymalna warotsc elementow
		int a;					//wartosci pomocnicze

		myfile.clear();
		myfile.seekg(0);

		myfile >> M;
		myfile >> N;

		cout << "Ilosc elementów: " << N << ' ' << endl << "maksymalna wartosc elementu: " << M << endl << endl;

		for (int i = 0; i < N; i++)				//wypelnianie stosu wartosciami z pliku
		{
			myfile >> a;						//czytanie wartosci z pliku	
			estos* newElement = new estos;
			newElement->wartosc = a;
			newElement->next = top;
			top = newElement;
		}

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);

		freq /= 1000; //ms

		pokastos(mystos);

		usunostatnistos(mystos);

		pokastos(mystos);

		usungornystos(mystos);

		pokastos(mystos);

		//szukaniestos(mystos);   niedziała

		czyszczeniestos(mystos);
		czyszczeniestos(tempstos);


		stosfile << to_string(elapsed) << "\t" << to_string(elapsed2) << "\t" << endl;  //wpisywanie danych do pliku csv

	}
	else {
		cout << "brak polaczenia z plikiem dane losowe" << endl;
	}
	myfile.close();								// zamknięcie pliku z danymi
}

struct ekol										//elementy kolejki
{
	int wartosc = 0;
	ekol* next = NULL;
};

ekol* mykol = new ekol;
ekol* glowa = mykol, * ogon = mykol;
ekol* tempkol = new ekol;
ekol* glowa2 = tempkol, * ogon2 = tempkol;

void usunopierwszykol(ekol*& glowa) {

	start = read_QPC();									//poczatek timera

	ekol* temp = glowa;									//pierwszy element 
	glowa = glowa->next;								// 2element zostaje głową
	delete temp;										//usuwanie pierwszego 

	elapsed4 = (read_QPC() - start) / (float)(freq);	//koniec timera
}

void usunostatnikol(ekol*& glowa)
{
	start = read_QPC();									//poczatek timera

	do {												//zdejmuje az do ostatniego elementu kol 1

		ekol* newElement = new ekol;					//tworzy newelement 
		newElement->wartosc = glowa->wartosc;			//przepisuje wartosc z kolejki 1 do newelement
		newElement->next = glowa->next;					//łaczy z 2ga kolejką
		ogon2->next = newElement;						//dodaje do 2kolejki
		ogon2 = ogon2->next;
		ogon2->next = NULL;

		ekol* temp3 = glowa;							//usuwa gorny element kolejki
		glowa = glowa->next;
		delete temp3;

	} while (glowa->next != NULL);

	ekol* temp3 = glowa2;								//usuwa gorny element kolejki
	glowa2 = glowa2->next;
	delete temp3;

	while (glowa2 != NULL) {							//kol 1 jest zapełniana wartościami z kol 2

		ekol* newElement2 = new ekol;					//tworzy newelement 
		newElement2->wartosc = glowa2->wartosc;			//przepisuje wartosc zkolejki 1 do newelement
		newElement2->next = NULL;						//łaczy z 2gą kolejką
		ogon->next = newElement2;						//dodaje do 2kolejki
		ogon = ogon->next;
		ekol* temp4 = glowa2;							//usuwa gorny element kolejki
		glowa2 = glowa2->next;
		delete temp4;
	}

	ekol* temp4 = glowa;								//usuwa gorny element kolejki
	glowa = glowa->next;
	delete temp4;
	elapsed5 = (read_QPC() - start) / (float)(freq);	//koniec timera

}

void szukaniekol(ekol* glowa) {
	int bi = 1, ba = 1;
	start = read_QPC();									//poczatek timera

	while (glowa != NULL) {

		if (glowa->wartosc == bi) {						//sprawdza czartosc szukana jest na danycm miejscu
														//cout itd
		}
		ba++;

		ekol* newElement = new ekol;					//tworzy newelement 
		newElement->wartosc = glowa->wartosc;			//przepisuje wartosc z kolejki 1 do newelement
		newElement->next = glowa->next;					//łaczy z 2gą kolejką
		ogon2->next = newElement;
		ogon2 = ogon2->next;
		ogon2->next = NULL;

		ekol* temp3 = glowa;							//usuwa gorny element kolejki
		glowa = glowa->next;
		delete temp3;
	}

	while (glowa2 != NULL) {							//kol 1 jest zapełniana wartościami z kol 2

		ekol* newElement2 = new ekol;					//tworzy newelement 
		newElement2->wartosc = glowa2->wartosc;			//przepisuje wartosc z kolejki 1 do newelement
		newElement2->next = glowa2->next;				//łaczy z 2gim stosem
		ogon->next = newElement2;						//dodaje do 2sotsu
		ogon = ogon->next;
		ogon->next = NULL;

		ekol* temp4 = glowa2;							//usuwa gorny element kolejki
		glowa2 = glowa2->next;
		delete temp4;
	}

	elapsed6 = (read_QPC() - start) / (float)(freq);	 //koniec timera
}

void czyszczeniekol(ekol*& glowa) {					//niedziała

	if (glowa != NULL) {
		ekol* temp = glowa;
		glowa = glowa->next;
		delete temp;
	}

}

void pokakolej(ekol* glowa) {

	while (glowa != NULL) {								// wypisuje do poki jest cos na stosie 
		cout << glowa->wartosc << endl;
		glowa = glowa->next;
	}
	cout << "NULL" << endl << endl;
}

void kol(ekol*& ogon)
{
	myfile.open("DaneLosowe.txt");

	if (myfile)
	{
		int N;					//elemnty 
		int M;					//maksymalna warotsc elementow
		int a;					//wartosci pomocnicze

		myfile.clear();
		myfile.seekg(0);

		myfile >> M;
		myfile >> N;

		cout << "Ilosc elementow: " << N << ' ' << endl << "maksymalna wartosc elementu: " << M << endl << endl;


		for (int i = 0; i < N; i++)				//wypelnianie stosu wartosciami z pliku
		{										//dodawanie elementu na koniec kolejki
			myfile >> a;						//czytanie wartosci z pliku					
			ekol* newElement = new ekol;
			newElement->wartosc = a;
			newElement->next = ogon;
			ogon = newElement;
		}

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		freq /= 1000; //ms

		cout << "kolejka z wpisanymi wartościami z pliku" << endl;
		pokakolej(mykol);

		usunopierwszykol(mykol);
		cout << "kolejka po usunieciu pierwszego elementu" << endl;
		pokakolej(mykol);

		usunostatnikol(mykol);
		cout << "kolejka po usunieciu ostatniego elementu" << endl;
		pokakolej(mykol);

		szukaniekol(mykol);

		pokakolej(mykol);
		//czyszczeniekol(mykol);
		//czyszczeniekol(tempkol);
													//zapisywanie pomiarów czasu
		kolfile << to_string(elapsed4) << "\t" << to_string(elapsed5) << "\t" << to_string(elapsed6) << endl;

	}
	else {
		cout << "brak polaczenia z plikiem dane losowe" << endl;
	}
	myfile.close();							// zamknięcie pliku z danymi
}

int main()
{
	//stosfile.open("PomiaryStos.csv");		//otwarcie plików z pomiarami
	kolfile.open("PomiaryKolejka.csv");

	U = 10;									//ilość elementów 

	for (int i = 0; i < 1; i++) {			//ilośc iteracji pomiarów 
		Wpisz();

		kol(mykol);

		//stos(mystos);

	}
	//stosfile.close();
	kolfile.close();

	return 0;
}