﻿#include <iostream>
#include <Windows.h>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

fstream myfile;
ofstream stosfile;
ofstream kolfile;

double elapsed4, elapsed5, elapsed6, elapsed, elapsed2, elapsed3, start;
long long int freq = 1000;

long long int read_QPC()
{
    LARGE_INTEGER count;
    QueryPerformanceCounter(&count);
    return ((long long int)count.QuadPart);
}

struct estos
{
    int wartosc = 0;
    estos* next = NULL;
};

estos* mystos;
estos* tempstos;

void Wpisz()
{
    int N = U;
    int M = 10000;
    int a;

    srand(time(NULL));

    myfile.open("DaneLosowe.txt", fstream::out);
    myfile.close();
    myfile.open("DaneLosowe.txt");

    myfile << M << ' ' << N << endl;

    int max = M;
    for (int i = 1; i <= N; i++)
    {
        a = rand() % max;
        myfile << a << endl;
    }

    myfile.close();
}

void usungornystos(estos*& top)
{
    start = read_QPC();
    estos* temp = top;
    top = (top)->next;
    delete temp;
    elapsed2 = (read_QPC() - start) / (float)(freq);
}

void pokastos(estos* top)
{
    while (top != NULL)
    {
        cout << top->wartosc << endl;
        top = top->next;
    }
    cout << "NULL" << endl << endl;
}

void usunostatnistos(estos*& top)
{
    start = read_QPC();
    do
    {
        estos* newElement = new estos;
        newElement->wartosc = top->wartosc;
        newElement->next = tempstos;
        tempstos = newElement;

        estos* temp = top;
        top = top->next;
        delete temp;

    } while (top->next != NULL);

    estos* temp = top;
    top = top->next;
    delete temp;
    top = NULL;

    while (tempstos != NULL)
    {
        estos* newElement = new estos;
        newElement->wartosc = tempstos->wartosc;
        newElement->next = top;
        top = newElement;

        estos* temp = tempstos;
        tempstos = tempstos->next;
        delete temp;
    }
    elapsed = (read_QPC() - start) / (float)(freq);
}

void czyszczeniestos(estos*& top)
{
    while (top != NULL)
    {
        estos* temp = top;
        top = top->next;
        delete temp;
    }
}

void stos(estos*& top)
{
    myfile.open("DaneLosowe.txt");

    if (myfile)
    {
        int N;
        int M;
        int a;

        myfile.clear();
        myfile.seekg(0);

        myfile >> M;
        myfile >> N;

        cout << "Ilosc elementów: " << N << ' ' << endl << "maksymalna wartosc elementu: " << M << endl << endl;

        for (int i = 0; i < N; i++)
        {
            myfile >> a;
            estos* newElement = new estos;
            newElement->wartosc = a;
            newElement->next = top;
            top = newElement;
        }
    }
    else
        cout << "Blad otwarcia pliku!" << endl;

    myfile.close();
}

void usungornakol(queue<int>& kol)
{
    start = read_QPC();
    kol.pop();
    elapsed3 = (read_QPC() - start) / (float)(freq);
}

void wstawdokol(queue<int>& kol, int a)
{
    start = read_QPC();
    kol.push(a);
    elapsed4 = (read_QPC() - start) / (float)(freq);
}

void pokazkol(queue<int> kol)
{
    while (!kol.empty())
    {
        cout << kol.front() << endl;
        kol.pop();
    }
    cout << "NULL" << endl << endl;
}

void usunostatnikol(queue<int>& kol)
{
    start = read_QPC();
    int N = kol.size();

    for (int i = 0; i < N - 1; i++)
    {
        kol.push(kol.front());
        kol.pop();
    }
    kol.pop();
    elapsed5 = (read_QPC() - start) / (float)(freq);
}

void czyszczeniekol(queue<int>& kol)
{
    while (!kol.empty())
        kol.pop();
}

void kolejka(queue<int>& kol)
{
    myfile.open("DaneLosowe.txt");

    if (myfile)
    {
        int N;
        int M;
        int a;

        myfile.clear();
        myfile.seekg(0);

        myfile >> M;
        myfile >> N;

        cout << "Ilosc elementów: " << N << ' ' << endl << "maksymalna wartosc elementu: " << M << endl << endl;

        for (int i = 0; i < N; i++)
        {
            myfile >> a;
            kol.push(a);
        }
    }
    else
        cout << "Blad otwarcia pliku!" << endl;

    myfile.close();
}

int main()
{
    stosfile.open("Stos.txt", fstream::out);
    kolfile.open("Kolejka.txt", fstream::out);

    stosfile << "Ilosc elementów: " << U << endl;
    kolfile << "Ilosc elementów: " << U << endl;

    stosfile << "Operacja" << setw(15) << "Czas [s]" << endl;
    kolfile << "Operacja" << setw(15) << "Czas [s]" << endl;

    stosfile.close();
    kolfile.close();

    for (int i = 0; i < 10; i++)
    {
        stosfile.open("Stos.txt", fstream::app);
        kolfile.open("Kolejka.txt", fstream::app);

        stosfile << endl << i + 1 << ". ";
        kolfile << endl << i + 1 << ". ";

        mystos = NULL;
        stos(mystos);

        stosfile << "Wstawianie na stos" << setw(7);
        czyszczeniestos(mystos);
        stos(mystos);

        start = read_QPC();
        stosfile << "push" << setw(14);
        for (int i = 0; i < U; i++)
            wstawdokol(mystos, rand() % 10 + 1);
        stosfile << elapsed4 << endl;

        stosfile << "Usuniecie ze stosu" << setw(4);
        start = read_QPC();
        usunostatnistos(mystos);
        stosfile << "pop" << setw(15) << elapsed << endl;

        stosfile << "Sprawdzenie zawartosci stosu" << setw(2);
        start = read_QPC();
        pokastos(mystos);
        stosfile << "pokaz" << setw(12) << elapsed6 << endl;

        stosfile << "Usuniecie gornego elementu stosu" << setw(2);
        start = read_QPC();
        usungornystos(mystos);
        stosfile << "pop" << setw(15) << elapsed2 << endl;

        stosfile << "Czyszczenie stosu" << setw(8);
        start = read_QPC();
        czyszczeniestos(mystos);
        stosfile << "clear" << setw(13) << elapsed3 << endl;

        stosfile.close();

        queue<int> mykol;
        kolejka(mykol);

        kolfile << "Wstawianie do kolejki" << setw(4);
        czyszczeniekol(mykol);
        kolejka(mykol);

        start = read_QPC();
        kolfile << "push" << setw(13);
        for (int i = 0; i < U; i++)
            wstawdokol(mykol, rand() % 10 + 1);
        kolfile << elapsed4 << endl;

        kolfile << "Usuniecie z kolejki" << setw(5);
        start = read_QPC();
        usunostatnikol(mykol);
        kolfile << "pop" << setw(14) << elapsed5 << endl;

        kolfile << "Sprawdzenie zawartosci kolejki" << setw(1);
        start = read_QPC();
        pokazkol(mykol);
        kolfile << "pokaz" << setw(11) << elapsed6 << endl;

        kolfile << "Usuniecie pierwszego elementu kolejki" << setw(1);
        start = read_QPC();
        usungornakol(mykol);
        kolfile << "pop" << setw(14) << elapsed3 << endl;

        kolfile << "Czyszczenie kolejki" << setw(7);
        start = read_QPC();
        czyszczeniekol(mykol);
        kolfile << "clear" << setw(12) << elapsed2 << endl;

        kolfile.close();
    }

    return 0;
}
