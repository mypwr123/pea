﻿#include <iostream>
#include <time.h>
#include <windows.h>
#include <fstream>

using namespace std;

clock_t start, stop;							//zmienne do liczenia czasu 

int ile;										//zmienne pomocnicze
double czas;
int* pom;
int* pom2;

void sortowanie_babelkowe(int* tab, int n)
{
	for (int i = 1; i < n; i++)
	{
		for (int j = n - 1; j >= 1; j--)
		{
			if (tab[j] < tab[j - 1])
			{									//zamiana miejscami ku prawej stronie
				int bufor;
				bufor = tab[j - 1];
				tab[j - 1] = tab[j];
				tab[j] = bufor;
			}
		}
	}
}

void sortowanie_wybor(int* tablica3, int ile, int n)
{
	int mn_index;									//zmienna pomocnicza przechowująca indeks komórki 

	ile = n;
	for (int i = 0; i < n - 1; i++)
	{
		mn_index = i;
		for (int j = i + 1; j < n; j++)				//pętla wyszukuje najmniejszy element w podzbiorze nieposortowanym
			if (tablica3[j] < tablica3[mn_index])
			{
				mn_index = j;
			}
		//zamiana elementu najmniejszego w podzbiorze z pierwszą pozycją nieposortowaną
		swap(tablica3[i], tablica3[mn_index]);
	}
}

void sortowanie_wstawianie(int* tablica4, int ile, int n)
{
	int pom, j;
	for (int i = 1; i < n; i++)
	{
		//wstawienie elementu w odpowiednie miejsce
		pom = tablica4[i];
		j = i - 1;


		while (j >= 0 && tablica4[j] > pom)		//przesuwanie elementów większych od pom
		{
			tablica4[j + 1] = tablica4[j];
			--j;
		}
		tablica4[j + 1] = pom;					//wstawienie pom w odpowiednie miejsce
	}
}



void merge(int tablica5[], int sta, int srodek, int kon)	//sortowanie przez zcalanie
{
	int* pom = new int[(kon - sta)];
	int i = sta, j = srodek + 1, k = 0;

	while (i <= srodek && j <= kon)					//kopiujemy lewą i prawą część tablicy do tablicy pomocniczej
	{
		if (tablica5[j] < tablica5[i])
		{
			pom[k] = tablica5[j];
			j++;
		}
		else
		{
			pom[k] = tablica5[i];
			i++;
		}
		k++;
	}

	if (i <= srodek)								//scalenie dwóch podtablic pomocniczych i zapisanie ich do właściwej tablicy
	{
		while (i <= srodek)
		{
			pom[k] = tablica5[i];
			i++;
			k++;
		}
	}
	else
	{
		while (j <= kon)
		{
			pom[k] = tablica5[j];
			j++;
			k++;
		}
	}
	for (i = 0; i <= kon - sta; i++)
		tablica5[sta + i] = pom[i];					//tu występuje problem z nadpisywaniem pamięci chyba


}

void mergesort(int tablica5[], int sta, int kon)
{
	int srodek;

	if (sta != kon)
	{
		srodek = (sta + kon) / 2;
		mergesort(tablica5, sta, srodek);
		mergesort(tablica5, srodek + 1, kon);
		merge(tablica5, sta, srodek, kon);
	}
}


void quicksort(int* tablica, int lewy, int prawy)	//funkcja licząca quicksortem
{
	int v = tablica[(lewy + prawy) / 2];			// (pivot)
	int i, j, x;
	i = lewy;
	j = prawy;
	do
	{
		while (tablica[i] < v) i++;				//szukam elementu wiekszego lub rownego piwot stojacego
		while (tablica[j] > v) j--;				//szukam elementu mniejszego lub rownego pivot stojacego
		if (i <= j)
		{
			x = tablica[i];
			tablica[i] = tablica[j];
			tablica[j] = x;
			i++;
			j--;
		}
	} while (i <= j);
	if (j > lewy) quicksort(tablica, lewy, j);
	if (i < prawy) quicksort(tablica, i, prawy);
}


void porownywanie_tablic(int tablica[], int tablica2[], int tablica3[], int tablica4[], int tablica5[], int ile, int n)
{
	//sprawdzanie czy wszystkie wartości w tablicach są takiesame na tychsamych miejscach w tablicy (czyli czy są dobrze posortowane)
	int git = 0, git2 = 0, git3 = 0, git4 = 0, git5 = 0;

	for (int i = 0; i < ile; i++)
	{
		if (tablica[i] == tablica2[i])
		{
			git++;
		}
		if (tablica2[i] == tablica3[i])
		{
			git2++;
		}
		if (tablica3[i] == tablica4[i])
		{
			git3++;
		}
		if (tablica4[i] == tablica5[i])
		{
			git4++;
		}
		if (tablica5[i] == tablica[i])
		{
			git5++;
		}
	}

	cout << endl << "sprawdzanie czy tablice zostaly posortowane poprawnie" << endl;
	if (git == ile) { cout << "tablica1 jest poprawna " << endl; }
	else { cout << " tablica1 NIE jest poprawna " << endl; }
	if (git2 == ile) { cout << "tablica2 jest poprawna " << endl; }
	else { cout << " tablica2 NIE jest poprawna " << endl; }
	if (git3 == ile) { cout << "tablica3 jest poprawna " << endl; }
	else { cout << " tablica3 NIE jest poprawna " << endl; }
	if (git4 == ile) { cout << "tablica4 jest poprawna " << endl; }
	else { cout << " tablica4 NIE jest poprawna " << endl; }
	if (git5 == ile) { cout << "tablica5 jest poprawna " << endl; }
	else { cout << " tablica5 NIE jest poprawna " << endl; }

	// cout << git << endl << git2 << endl << git3 << endl << git4 << endl << git5 << endl;     //wypisywanie wartości pomocniczych

}


void reset(int tablica[], int tablica2[], int tablica3[], int tablica4[], int tablica5[], int ile, int n)
{													//funkcja resetująca tablice do stanu losowego
	//wczytywanie losowych liczb do tablicy
	for (int i = 0; i < ile; i++)
	{
		tablica[i] = rand() % 100000 + 1;
	}

	//przepisanie tablicy do tablicy2 itd
	for (int i = 0; i < ile; i++)
	{
		tablica2[i] = tablica[i];
	}
	for (int i = 0; i < ile; i++)
	{
		tablica3[i] = tablica[i];
	}
	for (int i = 0; i < ile; i++)
	{
		tablica4[i] = tablica[i];
	}
	for (int i = 0; i < ile; i++)
	{
		tablica5[i] = tablica[i];
	}
}

int main()
{
	cout << "Porownanie czasow sortowania " << endl;

	cout << "Ile losowych liczb w tablicy: ";

	cin >> ile;											//wprowadzanie ilości elementów w tablicy


														//dynamiczna alokacja tablic
	int* tablica;
	tablica = new int[ile];

	int* tablica2;
	tablica2 = new int[ile];

	int* tablica3;
	tablica3 = new int[ile];

	int* tablica4;
	tablica4 = new int[ile];

	int* tablica5;
	tablica5 = new int[ile];


	srand(time(NULL));									//inicjowanie generatora



	/*													//wypisywanie danej tablicy przed posortowaniem
			cout<<"Przed posortowaniem: "<<endl;
			for(int i=0; i<ile; i++)
			{
				cout<<tablica5[i]<<" ";
			}
	*/


	
//		ofstream myfile;								//otworzenie pliku z pomiarami
//		myfile.open("pomiary.csv");
	

	//for (int i = 0; i < 5; i++) {						// pętla powtarzająca wywołania sortowań i zapisytania pomiarów do pliku 
	//	cout << i << endl;

	reset(tablica, tablica2, tablica3, tablica4, tablica5, ile, 0);			// funkcja resetująca tabele do stanu losowego

	//cout << "Sortuje teraz babelkowo. Prosze czekac!" << endl;				//info co sie dzieje w danej chwili
	start = clock();														//start pomiaru czasu
	sortowanie_babelkowe(tablica, ile);										//wywoływanie funkci 
	stop = clock();															//zakończenie pomiaru czasu
	czas = (double)(stop - start) / CLOCKS_PER_SEC;							//obliczanie różnicy czasu	
	//myfile << "bobelkowy:" << "\t" << czas << "\t" << "s" << "\t" ;		//zapisywanie pomiarów 
	cout << endl << "Czas sortowania babelkowego: " << czas << " s" << endl;//wwpisanie czasu na ekranie
	
	cout << endl << "Sortuje teraz algorytmem wybor. Prosze czekac!" << endl;
	start = clock();
	sortowanie_wybor(tablica3, 0, ile);
	stop = clock();
	czas = (double)(stop - start) / CLOCKS_PER_SEC;
	//myfile << "wybor:" << "\t" << czas << "\t" << "s" << "\t"  ;
	cout << endl << "Czas sortowania wybor: " << czas << " s" << endl;

	cout << endl << "Sortuje teraz algorytmem wstawianie. Prosze czekac!" << endl;
	start = clock();
	sortowanie_wstawianie(tablica4, 0, ile);
	stop = clock();
	czas = (double)(stop - start) / CLOCKS_PER_SEC;
	//myfile << "wstawianie:" << "\t" << czas << "\t" << "s" << "\t" ;
	cout << endl << "Czas sortowania wstawianie: " << czas << " s" << endl;


	cout << endl << "Sortuje teraz algorytmem quicksort. Prosze czekac!" << endl;
	start = clock();
	quicksort(tablica2, 0, ile - 1);
	stop = clock();
	czas = (double)(stop - start) / CLOCKS_PER_SEC;
	//myfile << "quicksort:" << "\t" << czas << "\t" << "s" << "\t";
	cout << endl << "Czas sortowania quicksort: " << czas << " s" << endl;


	//cout << endl << "Sortuje teraz algorytmem scalanie. Prosze czekac!" << endl;
	start = clock();
	mergesort(tablica5, 0, ile - 1);
	stop = clock();
	czas = (double)(stop - start) / CLOCKS_PER_SEC;
	//myfile << "scalanie:" << "\t" << czas << "\t" << "s" << endl;
	cout << endl << "Czas sortowania scalanie: " << czas << " s" << endl;

	
	//}      //   zakończenie pętli 100razy


	porownywanie_tablic(tablica, tablica2, tablica3, tablica4, tablica5, ile, 0);	//sprawdzenie czy po posortowaniu są jednakowe



	/*																//wypisanie danej tablicy po posortowaniu
			  cout<<"Po posortowaniu: "<<endl;
			   for(int i=0; i<ile; i++)
			   {
				   cout<<tablica5[i]<<" ";
			   }

	*/

	delete[] tablica;										//usuwanie tablic
	delete[] tablica2;
	delete[] tablica3;
	delete[] tablica4;
	delete[] tablica5;
	delete[] pom;
	delete[] pom2;

	cout << endl << "wykonal Oskar Waleszkiewicz" << endl << "index 254486" << endl;

	//myfile.close();											//zamykanie pliku z pomiarami

	return 0;
}
