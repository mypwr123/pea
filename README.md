BFS DFS - porównania czasu działania algorytmów BFS i DFS przeszukujących graf z użyciem macierzy sąsiadów dla różnych wielkości i gęstosci grafu C++<br>
rodzaje sortowania - porównanie czasu działania pięciu algorytmów sortujących wartości liczbowe w tablicy dla różnych wielkosci tabicy C++<br>
Stack vs Queue  - parównanie czasu operacji stosu i kolejki fifo vs lifo w C++<br>

BFS DFS - comparison of the execution time of BFS and DFS algorithms for graph traversal using an adjacency matrix for graphs of different sizes and densities in C++<br>
rodzaje sortowania (sorting algorithms) - a comparison of the execution time of five sorting algorithms for numerical values in an array for different array sizes in C++<br>
Stack vs Queue - a comparison of the operation time between a stack and a queue fifo vs lifo in C++<br>
